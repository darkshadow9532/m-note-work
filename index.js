const express = require('express')
const path = require('path')
const PORT = process.env.PORT || 5000
var bodyParser = require('body-parser');
var route = require("./route");
var restful = require('node-restful');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const uri = "mongodb+srv://fin:asrkpvg7@cluster0-ogucd.mongodb.net/work?retryWrites=true&w=majority";
mongoose.connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log("Successfully connected to the database");
    // mongoose.close();
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function callback () {
  console.log("h");
});

var app = express();
// app.use(express.static(path.join(__dirname, 'public')));
app.use('/static', express.static('public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ // to support URL-encoded bodies
    extended: true
}));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs')
app.get("/", (req, res)=>{
    res.render("note/note");
})
app.get("/new", (req, res)=>{
    res.render("note/new");
})
app.get("/all", (req, res)=>{
    res.render("note/all");
})
app.get("/m_all", (req, res)=>{
    res.render("note/m_all");
})

require('./app/routes/note.routes.js')(app);

// Minh begin

require('./app/routes/mqtt.routes.js')(app);
require('./app/routes/work_template.routes.js')(app);
require('./app/routes/work.routes.js')(app);

var moment = require('moment');
moment.locale("vi");
moment().format();

// listen for requests

const mqtt = require('async-mqtt');

var server  = mqtt.connect('mqtt://cretabase.kbvision.tv:1883');

server.on('connect', function () {
    server.subscribe('minh', function (err) {
        if(!err) {
            client.publish('minh','hello');
        }
    })
})

server.on('message', function (topic, message) {
    // message is Buffer
    console.log(message.toString())
    //server.end()
});

const Works = require('./app/models/work.model.js');
const MQTTs = require('./app/models/mqtt.model.js');
function deadline_check(){
    console.log("Starting...")
    let query_work = Works.find();
    let query_mqtt = MQTTs.find();
    let promise_work = query_work.exec();
    let promise_mqtt = query_mqtt.exec();
    Promise.all(
        [
            Promise.resolve(promise_work),
            Promise.resolve(promise_mqtt)
        ]
    )
    .then(function(result){
        //console.log(result);
        let works = result[0];
        let works_true = [];
        for (let i in works){
            if (Math.round(moment().diff(works[i].time, 'minutes', true)) === 0 && (works[i].done === false)){
                works_true.push(works[i]);
            }
        }
        console.log("works_true:", works_true);
        for (let i in works_true){
            
            let mqttId = works_true[i].action;
            var mqtt_client;
            let mqtts = result[1];
            console.log("mqttId",mqttId);
            //  console.log("mqtts", mqtts);

            
            for (let j in mqttId){
                for (let k in mqtts){
                    if (mqtts[k]._id == mqttId[j]){
                        let client_info = "";
                        console.log(mqtts[k].port);
                        if (mqtts[k].port != ""){
                            
                            client_info = "mqtt://" + mqtts[k].host + ":" + mqtts[k].port;

                        }
                        else {
                            client_info  = "mqtt://" + mqtts[k].host;
                        }
                        console.log(client_info);
                        let client  = mqtt.connect(client_info);
                        //console.log(client);
                        client.on('connect', function () {
                            console.log('connect');                            
                            client.publish(mqtts[k].topic, works_true[i].message);
                            console.log(mqtts[k].topic, works_true[i].message);
                            client.end();
                        });
                    }
                }             
                    
            }
                
        }
    })
    
    .catch(err => {
        console.log(err);
        // res.status(500).send({
        //     message: err.message || "Something went wrong"
        // });
    });
}

setInterval(deadline_check, 60000);

// Minh end

app.listen(PORT);