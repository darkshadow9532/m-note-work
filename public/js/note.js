console.log("Hello World")
var app = angular.module("app", []);
app.controller("note", function($scope, $http, $interval, $timeout) {
    
});

app.controller("all_note", function($scope, $http, $interval, $timeout) {
    $scope.begin = async function(){
        let note = new Note();
        let notes = await note.find();
        $scope.$apply(function(){
            $scope.notes = notes.filter(function(note){
                return note.status != "deleted";
            });
            // $scope.notes = notes;
        });
    }
    $scope.delete = async function(id){
        if(confirm("Bạn muốn xoá: " + id)){

        } else { return }
        let n = new Note(id);
        await n.begin();
        let d = n.get();
        try {
            d.status = "deactive";
        } catch(e){
            console.log(e);
        }
        n.set(d);
        await n.save();
        $scope.begin();
    }
    $scope.completed = async function(id){
        let n = new Note(id);
        let note = $scope.notes.filter((note)=>{
            return note._id == id;
        })
        console.log(note, id, $scope.notes);
        await n.begin();
        let d = n.get();
        // d.content = JSON.parse(d.content);
        console.log(JSON.parse(d.content));
        try {
            d.completed = note[0].completed;
        } catch(e){
            console.log(e);
        }
        console.log(d);
        n.set(d);
        await n.save();
        $scope.begin();
    }
    $scope.getDateString = function(strD){
        var d = new Date(strD);
        let str = d.getHours() + ":" + d.getMinutes() + " - " + d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear();
        return str;

    }
    $scope.filter = async function(){
        switch($scope.value_filter){
            case "completed":{
                await $scope.begin();
                $scope.$apply(function(){
                    $scope.notes = $scope.notes.filter(function(note){
                        return note.completed == true;
                    })
                });
            } break;
            case "deactive":{
                await $scope.begin();
                $scope.$apply(function(){
                    $scope.notes = $scope.notes.filter(function(note){
                        return note.status == "deactive";
                    })
                });
            } break;
            default:{
                await $scope.begin();
                $scope.$apply(function(){
                    $scope.notes = $scope.notes.filter(function(note){
                        return (note.completed != true) & (note.status != "deactive");
                    })
                });
            }
        }
    }

    $scope.begin();
});

app.controller("new_note", function($scope, $http, $interval, $timeout) {
    $scope.date = new Date();
    //$scope.m_date = moment();
    $scope.createNote = async function(){
        console.log($scope.date);
        if(($scope.content == "") || ($scope.content == undefined)){
            alert("Thêm nội dung vào đi!. :v")
            return;
        }
        if(($scope.date == "") || ($scope.date == undefined)){
            alert("Khi nào nó diễn ra?!. :v")
            return;
        }
        let obj = {
            content: $scope.content,
            title: $scope.title,
            completed: false,
            endTime: $scope.date,
            status: "active"
        }
        console.log(obj);
        let n = new Note(obj);
        await n.begin();
        await n.save();
    }
});