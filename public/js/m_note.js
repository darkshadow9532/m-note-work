console.log("Hello World")
var mqtt_action = "5dc22e457bd1d9162c6478b2";

var app = angular.module("app", []);
app.controller("note", function($scope, $http, $interval, $timeout) {
    
});

//var view_choice = ["new","near","end","done","undone"];
// app.controller("notes_login", function($scope,$http,$interval,$time){
//     $scope.username = "";
//     $scope.password = "";
//     $scope.login = function(username){
//         localStorage.setItem("username", $scope.username);
//         location.replace("/m_all");
//     }
// });
$(document).ready(function(){
    $("#sidebar_side").css("display","none");
    $("#new_note").css("display","none");
})

app.controller("notes_view", async function($scope,$http,$interval,$timeout){
    
    $scope.view_choice = "";
    $scope.attr_view_choice = "";
    $scope.notes_view = [];

    $scope.find_completed = function(notes, status){
        let new_notes = [];
        for (i in notes){
            if (notes[i].completed === status){
                new_notes.push(notes[i]);
            }
        }
        return new_notes;
    }
    $scope.find_status = function(notes, status){
        let new_notes = [];
        for (i in notes){
            if (notes[i].view_status == status){
                new_notes.push(notes[i]);
            }
        }
        return new_notes;
    }
    $scope.check_status = function(notes){
        for (i in notes ){
            if(moment().diff(moment(notes[i].endTime)) >= 0 ){
                notes[i].view_status = "end";
            }
            else if (moment().diff(moment(notes[i].remindTime)) >= 0 ){
                notes[i].view_status = "near";
            }
            else {
                notes[i].view_status = "new"
            }
        }
        return notes;
    }

    $scope.delete_work = function(id){
        //console.log(id);
        for (let i in $scope.works){
            if(id == $scope.works[i].parentId){
                let work_id = $scope.works[i]._id;
                //console.log(work_id);
                $http.delete("/works/" + work_id).then(function(res){
                    console.log(res);
                }).catch(function(err){
                    console.log(err);
                });
            }
        }
    }
    $scope.completed_work = function(id){
        for (let i in $scope.works){
            if(id == $scope.works[i].parentId){
                let work_id = $scope.works[i]._id;
                let work_obj = {
                    action:  $scope.works[i].action,
                    name:  $scope.works[i].name,
                    parent:  $scope.works[i].parent,
                    parentId:  $scope.works[i].parentId,
                    time:  - moment().diff($scope.date, 'minutes', true),
                    message:  $scope.works[i].message,
                    done: !$scope.works[i].done
                }
                
                console.log(work_obj);
                $http.put("/works/" + work_id, work_obj).then(function(res){
                    console.log(res);
                }).catch(function(err){
                    console.log(err);
                });
            }
        }

    }

    $scope.delete = async function(id){
        if(confirm("Bạn muốn xoá: " + id)){

        } else { return }
        let n = new Note(id);
        await n.begin();
        let d = n.get();
        try {
            d.status = "deactive";
        } catch(e){
            console.log(e);
        }
        n.set(d);
        await n.save();
        console.log(d);
        $scope.delete_work(id);

        await $scope.begin($scope.view_choice);
        $scope.view($scope.attr_view_choice);
    }

    $scope.completed = async function(id){
        let n = new Note(id);
        // let note = $scope.notes.filter((note)=>{
        //     return note._id == id;
        // })
        // console.log(note, id, $scope.notes);
        await n.begin();
        let d = n.get();
        // d.content = JSON.parse(d.content);
        //console.log(JSON.parse(d.content));
        try {
            d.completed = !d.completed;
        } catch(e){
            console.log(e);
        }
        console.log(d);
        n.set(d);
        await n.save();
        
        $scope.completed_work(id);
        await $scope.begin($scope.view_choice);
        $scope.view($scope.attr_view_choice);
    }

    $scope.view = function(view){
        $scope.attr_view_choice = view;
        console.log(view);
        $("#new_note").css("display","none");
        $("#view_note").css("display","initial");
        //console.log($scope.all_notes);
        $scope.$evalAsync(function(){
            
            switch(view){
                case "new":
                    $scope.notes_view = $scope.new_notes;
                    break;
                case "near":
                    $scope.notes_view = $scope.near_notes;
                    break;
                case "end":
                    $scope.notes_view = $scope.end_notes;
                    break;
                case "done":
                    $scope.notes_view = $scope.done_notes;
                    break;
                case "undone":
                    $scope.notes_view = $scope.undone_notes;
                    break;
                case "all":
                    $scope.notes_view = $scope.all_notes;
                    break;
                default:
                    $scope.notes_view = $scope.undone_notes;
            }
            console.log($scope.notes_view);    
        });
    }
    $scope.begin = async function(view_choice){
        console.log(view_choice);
        let note = new Note();
        let notes = await note.find({status: "active", source: view_choice});
        $scope.all_notes = $scope.check_status(notes);
        
        console.log($scope.all_notes);
        let undone_notes = $scope.find_completed($scope.all_notes, false);
        let done_notes = $scope.find_completed($scope.all_notes, true);
        
        let work = new Work();
        let works = await work.find();
        $scope.works = works;
        $scope.undone_notes = $scope.check_status(undone_notes);
        $scope.done_notes = $scope.check_status(done_notes);
        $scope.new_notes = $scope.find_status($scope.all_notes, "new");
        $scope.near_notes = $scope.find_status($scope.all_notes, "near");
        $scope.end_notes = $scope.find_status($scope.all_notes, "end");
        console.log("undone", $scope.undone_notes);
        console.log("done", $scope.done_notes);
        console.log("new", $scope.new_notes);
        console.log("near", $scope.near_notes);
        console.log("end", $scope.end_notes);
    }
    $scope.open = async function(view_choice){
        $("#sidebar_side").css("display","initial");
        await $scope.begin(view_choice);
        $scope.view("undone");
        localStorage.setItem("username", $scope.view_choice);
    }
    
    //await $scope.begin();

    // $scope.$apply(function(){
    //     $scope.notes_view = $scope.check_status($scope.notes_view);
    //     console.log($scope.notes_view);
    // })
    
    // $scope.view = function(view){
        
    //     switch(view){
    //         case "new":
    //                 $scope.notes_view = $scope.new_notes;
    //                 console.log("new");
    //             break;
    //         case "near":
    //                 $scope.notes_view = $scope.near_notes;
    //                 console.log("new");
    //             break;
    //         case "end":
    //                 $scope.notes_view = $scope.end_notes;
    //                 console.log("new");
    //             break;
    //         case "done":
                
    //                 $scope.notes_view = $scope.done_notes;
    //                 console.log("new");
    //             break;
    //         case "undone":
                
    //                 $scope.notes_view = $scope.undone_notes;
    //                 console.log("new");
    //             break;
    //         case "all":
                
    //                 $scope.notes_view = $scope.all_notes;
    //                 console.log("new");
    //             break;
    //         default:
                
    //                 $scope.notes_view = $scope.undone_notes;
    //                 console.log("new");
    //     }
    // }
    $scope.date = new Date();
    //$scope.m_date = moment();

    // Minh added
    $scope.new = function(){
        $("#new_note").css("display","initial");
        $("#view_note").css("display","none");
    }
    $scope.createWork = function(id){
        let work_obj = {
            action: mqtt_action,
            time: - moment().diff($scope.date, 'minutes', true),
            parent: "note",
            parentId: id,
            name: $scope.title,
            message: $scope.content,
            done: false,
        }
        console.log(work_obj);
        $http.post("/works", work_obj).then(function(res){
            console.log(res);
        }).catch(function(err){
            console.log(err);
        })
    }

    $scope.createNote = async function(){
        console.log($scope.date);
        if(($scope.content == "") || ($scope.content == undefined)){
            alert("Thêm nội dung vào đi!. :v")
            return;
        }
        if(($scope.date == "") || ($scope.date == undefined)){
            alert("Khi nào nó diễn ra?!. :v")
            return;
        }
        let obj = {
            content: $scope.content,
            title: $scope.title,
            source: $scope.view_choice,
            completed: false,
            remindTime: moment($scope.date).subtract(6,'hours'),
            endTime: $scope.date,
            status: "active"
        }
        console.log(obj);
        let n = new Note(obj);
        await n.begin();
        await n.save();
        let m = n.get();
        console.log(m);
        $scope.createWork(m._id);
        await $scope.begin($scope.view_choice);
        $scope.view($scope.attr_view_choice);
    }
    $scope.getDateString = function(time){
        let d = moment(time).format("hh:mma DD/MM/YY");
        return d;
    }
    $timeout(function(){
        if (typeof(Storage) !== "undefined"){
            $scope.view_choice = localStorage.getItem("username");
            console.log($scope.view_choice);
            $scope.open($scope.view_choice);
        }
    }, 500);
    
});