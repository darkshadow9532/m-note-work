const mongoose = require('mongoose');

const NoteSchema = mongoose.Schema({
    title: String,
    content: String,
    source: String,
    sourceId: String,
    status: String,
    completed: Boolean,
    remindTime: Date,
    endTime: Date
}, {
    timestamps: true
});

module.exports = mongoose.model('Note', NoteSchema);